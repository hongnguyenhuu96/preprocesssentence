
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * For write and read file purpose
 * @author hong
 */
public class FileIO {

    /**
     *
     * get all labels in file
     * 
     * @param file the file contains all labels
     * @return all labels in the file
     */
    public static ArrayList<Label> getLabelsFromFile(File file) {
        ArrayList<Label> label = new ArrayList<Label>();
        FileInputStream fileInPutStream = null;
        try {
            fileInPutStream = new FileInputStream(file);
            Reader reader = new java.io.InputStreamReader(fileInPutStream, "utf8");
            BufferedReader br = new BufferedReader(reader);
            Scanner sc = new Scanner(br);
            while (sc.hasNext()) {
                String nextLine = sc.nextLine();
                if (!"".equals(nextLine)) {
                    String[] labelComponents = nextLine.trim().split(":");
                    Label alabel = new Label(labelComponents[0].trim(), labelComponents[1].trim());
                    label.add(alabel);
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fileInPutStream.close();
            } catch (IOException ex) {
                Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return label;
    }

    /**
     *
     * get all categories from file
     * 
     * @param file the file contains all categories on the computer
     * @return all categories in the file (in type ::Thethao for each category)
     */
    public static ArrayList<String> getCategoriesFromFile(File file) {
        ArrayList<String> category = new ArrayList<String>();
        FileInputStream fileInPutStream = null;
        try {
            fileInPutStream = new FileInputStream(file);
            Reader reader = new java.io.InputStreamReader(fileInPutStream, "utf8");
            BufferedReader br = new BufferedReader(reader);
            Scanner sc = new Scanner(br);
            while (sc.hasNext()) {
                String nextLine = sc.nextLine();
                if (!"".equals(nextLine)) {
                    category.add("::" + nextLine);
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fileInPutStream.close();
            } catch (IOException ex) {
                Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return category;
    }

    /**
     * get all sentence from file
     * 
     * @param file name of the file contains sentence (query)
     * @param typeFile the tagged file to continue or the source file. 0: source file, 1: tagged file
     * @return an ArrayList of Sentences from file
     */
    public static ArrayList<Sentence> getSentecnesFromFile(File file, int typeFile) {
        ArrayList<Sentence> sentence = new ArrayList<Sentence>();
        FileInputStream fileInPutStream = null;
        try {
            fileInPutStream = new FileInputStream(file);
            Reader reader = new java.io.InputStreamReader(fileInPutStream, "utf8");
            BufferedReader br = new BufferedReader(reader);
            Scanner sc = new Scanner(br);
            while (sc.hasNext()) {
                String nextLine = sc.nextLine();
                if (!"".equals(nextLine)) {
                    if (typeFile == 0) {
                        String[] sentenceComponent = nextLine.split("::");
                        if(sentenceComponent.length == 2){
                            sentence.add(new Sentence(sentenceComponent[0].trim(), "", "::" + sentenceComponent[1].trim()));
                        }
                        if(sentenceComponent.length == 1){
                            sentence.add(new Sentence(sentenceComponent[0].trim(), "", ""));
                        }
                    }
                    if (typeFile == 1) {
                        String[] sentenceComponent = nextLine.split("###|\\::"); // status###content::category
                        if(sentenceComponent.length == 2){
                            sentence.add(new Sentence(sentenceComponent[1].trim(), sentenceComponent[0].trim(), ""));
                        }else{
                            sentence.add(new Sentence(sentenceComponent[1].trim(), sentenceComponent[0].trim(), "::" + sentenceComponent[2].trim()));
                        }
                    }
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fileInPutStream.close();
            } catch (IOException ex) {
                Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return sentence;
    }
    /**
     * get content of any file
     * 
     * @param file file that want to get content
     * @return content of the file
     */
    public static String getContent(File file) {
        String content = "";
        FileInputStream fileInPutStream = null;
        try {
            fileInPutStream = new FileInputStream(file);
            Reader reader = new java.io.InputStreamReader(fileInPutStream, "utf8");
            BufferedReader br = new BufferedReader(reader);
            Scanner sc = new Scanner(br);
            while (sc.hasNext()) {
                content += sc.nextLine() + "\n";
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fileInPutStream.close();
            } catch (IOException ex) {
                Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return content;
    }

    /**
     *
     * write content to specified file
     * 
     * @param file a file want to write
     * @param content a String contains the content to write
     * @return the content of the file
     */
    public static String writeFile(File file, String content) {
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(file);
            Writer writer = new java.io.OutputStreamWriter(fileOutputStream, "utf8");
            BufferedWriter bw = new BufferedWriter(writer);
            PrintWriter out = new PrintWriter(bw);

            out.print(content);
            out.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fileOutputStream.close();
            } catch (IOException ex) {
                Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return content;
    }

    /**
     *
     * write some labels to specified file
     * 
     * @param file the file contains all labels
     * @param label the label list want to write to label file
     * @return the content of the file
     */
    public static String writeLabelsFile(File file, ArrayList<Label> label) {
        String content = "";
        int size = label.size();
        for (int i = 0; i < size; i++) {
            Label aLabel = label.get(i);
            content += aLabel.name + ":" + aLabel.symbol + "\n";
        }
        writeFile(file, content);
        return content;
    }

    /**
     * 
     * write file to save the current state, (to continue in the next time (after quit program))
     * 
     * @param file contains labeled sentences
     * @param sentence (done###<act>mua</act> <obj>máy giặt</obj>::Công Nghệ)
     * @return the content of the file
     */
    public static String writeLabeledSentencesFile(File file, ArrayList<Sentence> sentence) {
        System.out.println(file.getName());
        System.out.println(file.getAbsolutePath());
        String content = "";
        int size = sentence.size();
        for (int i = 0; i < size; i++) {
            Sentence aSentence = sentence.get(i);
            content += aSentence.status + "###" + aSentence.content + aSentence.category + "\n";
        }
        writeFile(file, content);
        return content;
    }

    /**
     *
     * for "save and clean" purpose
     * 
     * @param file the terminate file
     * @param sentence (<act>mua</act> <obj>máy giặt</obj>::Công Nghệ)
     * @param type 1 write category
     * @return the content of the file
     */
    public static String writeClearFile(File file, ArrayList<Sentence> sentence) {
        String content = "";
        int size = sentence.size();
            for (int i = 0; i < size; i++) {
                content += sentence.get(i).content + sentence.get(i).category + "\n";
            }
        writeFile(file, content);
        return content;
    }
    /**
     * write categories to file to save
     * 
     * @param file the specified file
     * @param categories all categories that want to save
     * @return the content was written to file
     */
    public static String writeCategoriesFile(File file, ArrayList<String> categories) {
        String content = "";
        int size = categories.size();
        for (int i = 0; i < size; i++) {
            String []cate = categories.get(i).split("::");
            for(int j = 0; j< cate.length; j++){
                content += cate[j];
            }
            content += "\n";
        }
        writeFile(file, content);
        return content;
    }
}

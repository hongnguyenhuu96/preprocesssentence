
import com.sun.glass.events.KeyEvent;
import java.awt.GridLayout;
import java.io.File;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.Border;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Hong
 */
public class MainFrame extends javax.swing.JFrame {

    /**
     * Creates new form Frame
     */
    public MainFrame() {
        initComponents();
        btOpenSentence.requestFocus();
        this.setTitle("Preprocessing sentence program");
        if(labelFile != null) setLabel();
        if(categoryFile != null) setCategory();
        if(sentenceFile != null) {
            if (sentenceFile.getName().contains("tagged")) {
                taggedFile = sentenceFile;
                typeOfSourceFileInput = 1;
                setEditorAndTable(taggedFile, typeOfSourceFileInput);
            } else {
                sentenceFile = sentenceFile;
                taggedFile = new File("tagged_" + sentenceFile.getName());
                typeOfSourceFileInput = 0;
                setEditorAndTable(sentenceFile, typeOfSourceFileInput);
            }
        }
    }
    
    public ArrayList<Label> labels; // contains all labels read from file
    public ArrayList<String> categories; // contains all categories read from file
    public ArrayList<Sentence> sentences; // to save the state of sentence after add labels and categories to
    public ArrayList<Sentence> copySentences; // to save the initial state of sentences (right after read from file)
    public ArrayList<JCheckBox> cbLabel; // contains all check boxs for labels
    public ArrayList<JCheckBox> cbCategory; // contains all check box for categories
    static int sizeLabel; // the number of label
    static int sizeCategory; // the number of category
    static int sizeSentence; // the number of sentence
    int typeOfSourceFileInput; // type 0: sentences file, type 1: tagged file
    
    // some mode use to operate file
    private static final int OPEN_SENTENCE = 0;
    private static final int OPEN_LABEL = 1;
    private static final int OPEN_CATEGORY = 2;
    private static final int CLEAN_AND_SAVE = 3;
    
    int currentSentenceNum; // the current sentence (the content of current sentence show in text area)
    String STR_CATE = "::"; // if use category CATE = "::" else CATE = "" // delimeter to separate sentence and its categories
    String temp; // state of current sentence display in text area, it is used for undo function
    File labelFile = null; // labels file
    File categoryFile = null; // categories file
    File sentenceFile = null; // file contains all sentences need to be tagged
    File taggedFile = null; // file to save to current stage of all sentences in tagging process

//    File labelFile = new File("label.txt");
//    File categoryFile = new File("category.txt");
//    File sentenceFile = new File("sentence.txt");
//    File taggedFile = null;
    
    /**
     * regenerate labels(check box) for label panels and assign action for each of them
     */
    public void setLabel() {
        pnLabels.removeAll();
        labels = FileIO.getLabelsFromFile(labelFile);
        sizeLabel = labels.size();
        cbLabel = new ArrayList<>();
        createCbLabel();
        showCbLabel();
        setActionForAllCbLabel();
        pnLabels.doLayout();
    }
    
    /**
     * regenerate category(check box) for category panel and assign action for each of them
     */
    public void setCategory() {
        pnCategories.removeAll();
        categories = FileIO.getCategoriesFromFile(categoryFile);
        sizeCategory = categories.size();
        cbCategory = new ArrayList<>();
        createCbCategory();
        showCbCategory();
        setActionForAllCbCategory();
        pnCategories.doLayout();
    }

    public void setEditorAndTable(File file, int type) { // file tagged or not, tagged -> type 1, (tagged, 1) (normal, 0)
        sentences = FileIO.getSentecnesFromFile(file, type);
        copySentences = new ArrayList<>();
        cloneSentence(copySentences, sentences);
        sizeSentence = sentences.size();
        currentSentenceNum = 0;
        tpSentence.setText(sentences.get(currentSentenceNum).content + sentences.get(currentSentenceNum).category);
        temp = tpSentence.getText();
        loadTable(0);
    }
    
    /**
     * 
     * @param copy to save a copy of source
     * @param source the source contains all Sentences
     */
    public void cloneSentence(ArrayList<Sentence> copy, ArrayList<Sentence> source) {
        int size = source.size();
        for (int i = 0; i < size; i++) {
            Sentence aSentence = source.get(i);
            copy.add(new Sentence(aSentence.content, aSentence.status, aSentence.category));
        }
    }
    /**
     * create all check box label (correspond to ArrayList labels(read from label file))
     */
    public void createCbLabel() {
        sizeLabel = labels.size();
        for (int i = 0; i < sizeLabel; i++) {
            JCheckBox aCbLabel = new JCheckBox(labels.get(i).name);
            aCbLabel.setFont(new java.awt.Font("Tahoma", 1, 11));
            cbLabel.add(aCbLabel);
            btgrLabels.add(aCbLabel);
        }
    }
    
    /**
     * create all check box category (correspond to ArrayList categories (read from categories file))
     * but add remove button
     */
    public void createCbCategory() {
        sizeCategory = categories.size();
        for (int i = 0; i < sizeCategory; i++) {
            JCheckBox aCbCate = new JCheckBox(categories.get(i).substring(2));
            aCbCate.setFont(new java.awt.Font("Tahoma", 1, 11));
            cbCategory.add(aCbCate);
            btgrCategories.add(aCbCate);
        }
        // create remove checkbox and assign action
        cbRemoveCate = new JCheckBox("Remove!");
        btgrCategories.add(cbRemoveCate);
        cbRemoveCate.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                if (cbRemoveCate.isSelected()) {
                    temp = tpSentence.getText();
                    String[] text = temp.split(STR_CATE);
                    tpSentence.setText(text[0]);
                }
                btgrCategories.clearSelection();
            }
        });
    }
    JCheckBox cbRemoveCate;
    
    public void showCbLabel() {
        Border labelBorder = BorderFactory.createTitledBorder("Label");
        pnLabels.setBorder(labelBorder);
        GridLayout layout = new GridLayout(0, 3);
        layout.setHgap(0);
        layout.setVgap(0);
        pnLabels.setLayout(layout);

        for (int i = 0; i < sizeLabel; i++) {
            pnLabels.add(cbLabel.get(i));
        }
    }

    public void showCbCategory() {
        Border categoryBorder = BorderFactory.createTitledBorder("Category");
        pnCategories.setBorder(categoryBorder);
        GridLayout layout = new GridLayout(0, 3);
        layout.setHgap(0);
        layout.setVgap(0);
        pnCategories.setLayout(layout);

        for (int i = 0; i < sizeCategory; i++) {
            pnCategories.add(cbCategory.get(i));
        }
        pnCategories.add(cbRemoveCate);
    }
    /**
     * create table with that contains data from ArrayList sentences
     * @param i the selected row in the created table
     */
    public void loadTable(int i) {
        TableModel dataModel = new DefaultTableModel(loadRowData(), loadCollumName()) {
            public boolean isCellEditable(int row, int column) {
                return false; //This causes all cells to be not editable
            }
        };
        tbSentence.setModel(dataModel);

        tbSentence.getColumnModel().getColumn(0).setPreferredWidth(20);
        tbSentence.getColumnModel().getColumn(1).setPreferredWidth(700);
        tbSentence.getColumnModel().getColumn(2).setPreferredWidth(80);
        tbSentence.setRowSelectionInterval(i, i);
    }
    /**
     * load data for rows of table correspond to ArrayList sentences
     * @return dataInTable
     */
    public String[][] loadRowData() {
        sizeSentence = sentences.size();
        String dataInTable[][] = new String[sizeSentence][3];
        for (int i = 0; i < sizeSentence; i++) {
            Sentence aSentence = sentences.get(i);
            dataInTable[i][0] = i + 1 + "";
            dataInTable[i][1] = aSentence.content + aSentence.category;
            dataInTable[i][2] = aSentence.status;
        }
        return dataInTable;
    }
    /**
     * set collumName for the table
     * @return 
     */
    public String[] loadCollumName() {
        return new String[]{"No.", "Sentence", "Status"};
    }

    public void operateFile(String title, int type) {
        JFileChooser chooser = new JFileChooser();
        chooser.setDialogTitle(title);
        int approveOrNot = -1;
        switch (type) {
            case OPEN_LABEL:
            case OPEN_CATEGORY:
            case OPEN_SENTENCE:
                approveOrNot = chooser.showOpenDialog(null);
                break;
            case CLEAN_AND_SAVE:
                approveOrNot = chooser.showSaveDialog(null);
                break;
        }

        if (approveOrNot == JFileChooser.APPROVE_OPTION) {
            File file = chooser.getSelectedFile();
            switch (type) {
                case OPEN_LABEL:
                    labelFile = file;
                    setLabel();
                    btOpenCategory.requestFocus();
                    break;
                case OPEN_CATEGORY:
                    categoryFile = file;
                    setCategory();
                    btOpenSentence.requestFocus();
                    break;
                case OPEN_SENTENCE:
                    // from name of selected file decide the way to read file
                    //(0: source file(txt), 1: tagged file(after tag some sentence))
                    if (file.getName().contains("tagged")) {
                        taggedFile = file;
                        typeOfSourceFileInput = 1;
                        setEditorAndTable(taggedFile, typeOfSourceFileInput);
                    } else {
                        sentenceFile = file;
                        taggedFile = new File("tagged_" + file.getName());
                        typeOfSourceFileInput = 0;
                        setEditorAndTable(sentenceFile, typeOfSourceFileInput);
                    }
                    btOpenLabel.requestFocus();
                    break;
                case CLEAN_AND_SAVE:
                    // save all sentences that was tagged to new file in clear(status) format
                    FileIO.writeClearFile(file, sentences);
                    break;
            }
        }
    }
    
    /**
     * when click to another sentence in table
     * save the current sentence, and update text area(tpSentence) to the selected sentence
     * update the current text from text area to temp
     */
    public void actWithTable() {
        saveSentence(currentSentenceNum);
        currentSentenceNum = tbSentence.getSelectedRow();
        Sentence asentence = sentences.get(currentSentenceNum);
        loadTable(currentSentenceNum);
        tpSentence.setText(asentence.content + asentence.category);
        temp = tpSentence.getText();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btgrCategories = new javax.swing.ButtonGroup();
        btgrLabels = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        pnLabels = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tpSentence = new javax.swing.JEditorPane();
        pnCategories = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        btUndo = new javax.swing.JButton();
        btReStore = new javax.swing.JButton();
        btBack = new javax.swing.JButton();
        btNext = new javax.swing.JButton();
        btConsider = new javax.swing.JButton();
        btSaveAndClean = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbSentence = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        btAddLabel = new javax.swing.JButton();
        btOpenLabel = new javax.swing.JButton();
        btOpenSentence = new javax.swing.JButton();
        btOpenCategory = new javax.swing.JButton();
        btAddCategory = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setLayout(new java.awt.GridLayout(1, 0));

        javax.swing.GroupLayout pnLabelsLayout = new javax.swing.GroupLayout(pnLabels);
        pnLabels.setLayout(pnLabelsLayout);
        pnLabelsLayout.setHorizontalGroup(
            pnLabelsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 289, Short.MAX_VALUE)
        );
        pnLabelsLayout.setVerticalGroup(
            pnLabelsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 231, Short.MAX_VALUE)
        );

        jPanel1.add(pnLabels);

        tpSentence.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tpSentence.setToolTipText("");
        jScrollPane1.setViewportView(tpSentence);

        jPanel1.add(jScrollPane1);

        javax.swing.GroupLayout pnCategoriesLayout = new javax.swing.GroupLayout(pnCategories);
        pnCategories.setLayout(pnCategoriesLayout);
        pnCategoriesLayout.setHorizontalGroup(
            pnCategoriesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 289, Short.MAX_VALUE)
        );
        pnCategoriesLayout.setVerticalGroup(
            pnCategoriesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 231, Short.MAX_VALUE)
        );

        jPanel1.add(pnCategories);

        jPanel2.setLayout(new java.awt.GridLayout(1, 0));

        btUndo.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btUndo.setText("Undo");
        btUndo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btUndoActionPerformed(evt);
            }
        });
        jPanel2.add(btUndo);

        btReStore.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btReStore.setText("Restore");
        btReStore.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btReStoreActionPerformed(evt);
            }
        });
        jPanel2.add(btReStore);

        btBack.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btBack.setText("Back");
        btBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btBackActionPerformed(evt);
            }
        });
        jPanel2.add(btBack);

        btNext.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btNext.setText("Next");
        btNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btNextActionPerformed(evt);
            }
        });
        jPanel2.add(btNext);

        btConsider.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btConsider.setText("Un/Consider");
        btConsider.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btConsiderActionPerformed(evt);
            }
        });
        jPanel2.add(btConsider);

        btSaveAndClean.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btSaveAndClean.setText("Save and Clean");
        btSaveAndClean.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSaveAndCleanActionPerformed(evt);
            }
        });
        jPanel2.add(btSaveAndClean);

        tbSentence.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tbSentenceMousePressed(evt);
            }
        });
        tbSentence.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbSentenceKeyPressed(evt);
            }
        });
        jScrollPane2.setViewportView(tbSentence);

        jPanel4.setLayout(new java.awt.GridLayout(1, 0));

        btAddLabel.setText("Add/ Remove Label");
        btAddLabel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btAddLabelActionPerformed(evt);
            }
        });
        jPanel4.add(btAddLabel);

        btOpenLabel.setText("Chose File (Label)");
        btOpenLabel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btOpenLabelActionPerformed(evt);
            }
        });
        jPanel4.add(btOpenLabel);

        btOpenSentence.setText("Choose File (Sentence)");
        btOpenSentence.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btOpenSentenceActionPerformed(evt);
            }
        });
        jPanel4.add(btOpenSentence);

        btOpenCategory.setText("Choose File (Category)");
        btOpenCategory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btOpenCategoryActionPerformed(evt);
            }
        });
        jPanel4.add(btOpenCategory);

        btAddCategory.setText("Add/ Remove Category");
        btAddCategory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btAddCategoryActionPerformed(evt);
            }
        });
        jPanel4.add(btAddCategory);

        jLabel1.setFont(new java.awt.Font("Tahoma", 2, 11)); // NOI18N
        jLabel1.setText("Nguyễn Hữu Hồng - K59 CA - Đại học Công Nghệ - ĐHQGHN");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 869, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING)
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 303, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    /**
     * set text of text area (tpSentence) to the previous state
     * @param evt 
     */
    private void btUndoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btUndoActionPerformed
        if (taggedFile == null) {
            return;
        }
        // temp is the previous state and is updated when adding label or category
        tpSentence.setText(temp);
    }//GEN-LAST:event_btUndoActionPerformed
    /**
     * restore the state of current sentence to the first state (before modify anything)
     * @param evt 
     */
    private void btReStoreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btReStoreActionPerformed
        if (taggedFile == null) {
            return;
        }
        // sentences: contains modified sentences
        // copySentences: contains pured sentences (read from file)
        Sentence a = copySentences.get(currentSentenceNum);
        Sentence b = sentences.get(currentSentenceNum);
        tpSentence.setText(a.content + a.category);
        b.status = a.status;
        b.category = a.category;
        saveSentence(currentSentenceNum);
        loadTable(currentSentenceNum);
        temp = tpSentence.getText();
    }//GEN-LAST:event_btReStoreActionPerformed
    /**
     * when press next, save the current sentence state if the current sentence is modified
     * update text area (tpSentence) and table to the next sentence 
     * @param evt 
     */
    private void btNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btNextActionPerformed
        if (taggedFile == null) {
            return;
        }
        System.out.println(temp);
        System.out.println(tpSentence.getText());
        if (!temp.equals(tpSentence.getText())) {
            sentences.get(currentSentenceNum).status = "done";
            saveSentence(currentSentenceNum);
        }
        System.out.println(sentences.get(currentSentenceNum).status);
        if (currentSentenceNum == sentences.size() - 1) {
            loadTable(currentSentenceNum);
            return;
        };
        currentSentenceNum++;
        Sentence asentence = sentences.get(currentSentenceNum);
        loadTable(currentSentenceNum);
        tpSentence.setText(asentence.content + asentence.category);
        temp = tpSentence.getText();
    }//GEN-LAST:event_btNextActionPerformed
    
    /**
     * when press back, save the current sentence state if the current sentence is modified
     * update text area (tpSentence) and table to the back sentence 
     * @param evt 
     */
    private void btBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btBackActionPerformed
        if (taggedFile == null) {
            return;
        }
        // update current sentence state
        if (!temp.equals(tpSentence.getText())) {
            sentences.get(currentSentenceNum).status = "done";
            saveSentence(currentSentenceNum);
        }
        if (currentSentenceNum == 0) {
            loadTable(currentSentenceNum);
            return;
        };
        // change content of text area and update table
        currentSentenceNum--;
        Sentence asentence = sentences.get(currentSentenceNum);
        loadTable(currentSentenceNum);
        tpSentence.setText(asentence.content + asentence.category);
        temp = tpSentence.getText();
    }//GEN-LAST:event_btBackActionPerformed
    
    /**
     * when press consider button the status of current sentence should change to consider or unconsider
     * and save new state of current sentence and update table
     * @param evt 
     */
    private void btConsiderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btConsiderActionPerformed
        if (taggedFile == null) {
            return;
        }
        Sentence aSentence = sentences.get(currentSentenceNum);
        if (aSentence.status.equals("consider")) {
            aSentence.status = "done";
        }
        else if (aSentence.status.equals("done")) {
            aSentence.status = "";
        }
        else if (aSentence.status.equals("")) {
            aSentence.status = "consider";
        } else {
            aSentence.status = "";
        }
        
        saveSentence(currentSentenceNum);
        loadTable(currentSentenceNum);
    }//GEN-LAST:event_btConsiderActionPerformed
    
    /**
     * when click to "add label" button -> create new frame
     * if the label file is specified
     * it shows the current labels in label file to modify
     * @param evt 
     */
    private void btAddLabelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btAddLabelActionPerformed
        if (labelFile == null) {
            JOptionPane.showMessageDialog(rootPane, "You must choose label file first");
            return;
        }
        AddRemoveLabelAndCategoryFrame addRemoveLabelFrame = new AddRemoveLabelAndCategoryFrame();
        addRemoveLabelFrame.setVisible(true);
        addRemoveLabelFrame.setLabelFile(labelFile);
        addRemoveLabelFrame.setMainFrame(this);
    }//GEN-LAST:event_btAddLabelActionPerformed
    
    /**
     * when click to "add category" button -> create new frame
     * if the category file is specified
     * it shows the current categories in category file to modify
     * @param evt 
     */
    private void btAddCategoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btAddCategoryActionPerformed
        if (categoryFile == null) {
            JOptionPane.showMessageDialog(rootPane, "You must choose category file first");
            return;
        }
        AddRemoveLabelAndCategoryFrame addRemoveCateFrame = new AddRemoveLabelAndCategoryFrame();
        addRemoveCateFrame.setVisible(true);
        addRemoveCateFrame.setCategoryFile(categoryFile);
        addRemoveCateFrame.setMainFrame(this);
    }//GEN-LAST:event_btAddCategoryActionPerformed
    
    /**
     * call operate file with mode open_label
     * @param evt 
     */
    private void btOpenLabelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btOpenLabelActionPerformed
        operateFile("Open a file contains your all labels", OPEN_LABEL);
    }//GEN-LAST:event_btOpenLabelActionPerformed
    
    /**
     * call operateFile with mode open_sentence
     * @param evt 
     */
    private void btOpenSentenceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btOpenSentenceActionPerformed
        operateFile("Open a file contains your all sentences", OPEN_SENTENCE);
    }//GEN-LAST:event_btOpenSentenceActionPerformed
    
    /**
     * call operateFile() with mode open_category
     * @param evt 
     */
    private void btOpenCategoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btOpenCategoryActionPerformed
        operateFile("Open a file contains your all categories", OPEN_CATEGORY);
    }//GEN-LAST:event_btOpenCategoryActionPerformed

    /**
     * call operateFile() with mode clean_and_save
     * @param evt 
     */
    private void btSaveAndCleanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSaveAndCleanActionPerformed
        if(taggedFile == null) return;
        operateFile("Choose where to save your file", CLEAN_AND_SAVE);
    }//GEN-LAST:event_btSaveAndCleanActionPerformed
    
    /**
     * call actWithTable() when click to the table
     * @param evt 
     */
    private void tbSentenceMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbSentenceMousePressed
        actWithTable();
    }//GEN-LAST:event_tbSentenceMousePressed
    
    /**
     * update text area(tpSentence) when the position of
     * selected sentence in table is changed by press key (up, down, enter)  
     * @param evt 
     */
    private void tbSentenceKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbSentenceKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_DOWN){
            if(currentSentenceNum == sentences.size() - 1) return;
            currentSentenceNum++;
            Sentence a = sentences.get(currentSentenceNum);
            tpSentence.setText(a.content + a.category);
            temp = tpSentence.getText();
        }
        if(evt.getKeyCode() == KeyEvent.VK_UP){
            if(currentSentenceNum == 0) return;
            currentSentenceNum--;
            Sentence a = sentences.get(currentSentenceNum);
            tpSentence.setText(a.content + a.category);
            temp = tpSentence.getText();
        }
        if(evt.getKeyCode() == KeyEvent.VK_ENTER){
            currentSentenceNum = (currentSentenceNum + 1) % sentences.size();
            Sentence a = sentences.get(currentSentenceNum);
            tpSentence.setText(a.content + a.category);
            temp = tpSentence.getText();
        }
    }//GEN-LAST:event_tbSentenceKeyPressed
    /**
     * set action for all labels check box when click to
     * It will trigger function labelSentenceBySymbol to surround the selected text
     * in text area by symbol of the label
     */
    public void setActionForAllCbLabel() {
        int size = cbLabel.size();
        for (int i = 0; i < size; i++) {
            JCheckBox currentLabel = cbLabel.get(i);
            String symbol = labels.get(i).symbol;
            currentLabel.addItemListener(new java.awt.event.ItemListener() {
                public void itemStateChanged(java.awt.event.ItemEvent evt) {
                    if (currentLabel.isSelected()) {
                        labelSentenceBySymbol(symbol);
                    }
                    btgrLabels.clearSelection();
                }
            });
        }
    }
    /**
     * set action for all categories check box when click to
     * It will trigger function setCategoryforSentenceBy(cateName)
     * to append cateName to the current text area
     */
    public void setActionForAllCbCategory() {
        int size = cbCategory.size();
        for (int i = 0; i < size; i++) {
            JCheckBox currentCate = cbCategory.get(i);
            String cateName = categories.get(i);
            currentCate.addItemListener(new java.awt.event.ItemListener() {
                public void itemStateChanged(java.awt.event.ItemEvent evt) {
                    if (currentCate.isSelected()) {
                        setCategoryforSentenceBy(cateName);
                    }
                    btgrCategories.clearSelection();
                }
            });
        }
    }
    /**
     * append category to the current text in text area tpSentence
     * @param cateName 
     */
    private void setCategoryforSentenceBy(String cateName) {
        temp = tpSentence.getText();
//        String text[] = temp.split(STR_CATE); // version one category for a sentence
//        tpSentence.setText(text[0] + cateName);
        // version many categories for a sentence
        if(temp.contains(STR_CATE)){
            // if the sentence already has more than one category
            tpSentence.setText(temp + "#" + cateName.substring(2)); // "#" character to separate categories
        }else{
            // the sentence hasn't have any category before
            String text[] = temp.split(STR_CATE);
            tpSentence.setText(text[0] + cateName);
        }
    }
    /**
     * add label to selected text in text area: tpSentence
     * play -> <act>play</act>
     * @param typeLabel label like <act></act> for action, <prc></prc> for price
     */
    private void labelSentenceBySymbol(String typeLabel) {
        temp = tpSentence.getText();
        int selectionStart = tpSentence.getSelectionStart();
        int selectionEnd = tpSentence.getSelectionEnd();
        if (selectionStart == selectionEnd) {
            return;
        }
        while (temp.charAt(selectionStart) == ' ') {
            selectionStart++;
        }
        while (temp.charAt(selectionEnd - 1) == ' ') {
            selectionEnd--;
        }
        if (selectionStart == selectionEnd) {
            return;
        }
        StringBuilder strBuilder = new StringBuilder(tpSentence.getText());
        strBuilder.replace(selectionStart, selectionEnd, "<" + typeLabel + ">" + tpSentence.getSelectedText().trim() + "</" + typeLabel + ">");
        tpSentence.setText(strBuilder.toString());
        tpSentence.requestFocus();
        // set cusor to the next word
        int caretPosition = selectionEnd + 5 + typeLabel.length() * 2 + 1;
        if (caretPosition < tpSentence.getText().length()) {
            tpSentence.setCaretPosition(caretPosition);
        }
    }
    /**
     * set content of text area to sentences[i] and save to file
     * @param i index of sentence in ArrayList<Sentence> sentences that need to update
     */
    public void saveSentence(int i) { // save sentence to arraylist and file
        String[] text = tpSentence.getText().split(STR_CATE);
        Sentence a = sentences.get(i);
        if (text.length == 1) {
            a.content = text[0];
            a.category = "";
        }
        if (text.length == 2) {
            a.content = text[0];
            a.category = "::" + text[1];
        }
        FileIO.writeLabeledSentencesFile(taggedFile, sentences);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        try {
            //</editor-fold>
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btAddCategory;
    private javax.swing.JButton btAddLabel;
    private javax.swing.JButton btBack;
    private javax.swing.JButton btConsider;
    private javax.swing.JButton btNext;
    private javax.swing.JButton btOpenCategory;
    private javax.swing.JButton btOpenLabel;
    private javax.swing.JButton btOpenSentence;
    private javax.swing.JButton btReStore;
    private javax.swing.JButton btSaveAndClean;
    private javax.swing.JButton btUndo;
    private javax.swing.ButtonGroup btgrCategories;
    private javax.swing.ButtonGroup btgrLabels;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JPanel pnCategories;
    private javax.swing.JPanel pnLabels;
    private javax.swing.JTable tbSentence;
    private javax.swing.JEditorPane tpSentence;
    // End of variables declaration//GEN-END:variables
}
